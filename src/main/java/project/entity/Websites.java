package project.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "websites")
public class Websites {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "WEBSITE_ID")
    private long id;

    @Column(name = "WEBSITE_ADDRESS")
    private String websiteAddress;

    @Column(name = "WEBSITE_VALUE")
    private String websiteValue;

    public Websites() {
    }

    public Websites(String websiteAddress, String websiteValue) {
        this.websiteAddress = websiteAddress;
        this.websiteValue = websiteValue;
    }

    @Override
    public String toString() {
        return "Websites{" +
                "id=" + id +
                ", websiteAddress='" + websiteAddress + '\'' +
                ", websiteValue='" + websiteValue + '\'' +
                '}';
    }
}
