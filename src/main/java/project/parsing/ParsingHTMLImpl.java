package project.parsing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import project.entity.Websites;
import project.service.WebsiteService;
import project.service.WebsiteServiceImpl;
import project.utility.Utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ParsingHTMLImpl implements ParsingHTML {
    private final WebsiteService websiteService = new WebsiteServiceImpl();

    private static final Logger LOGGER = LogManager.getLogger();
    private Document doc;
    private String mapToJson;

    //Splitting a string with the html page content using a regular expression, and writing to map
    @Override
    public void splitAndWordCount(String url) {
        boolean isValid = Utils.urlValidation(url);

        if (isValid) {
            try {
                Utils.setTrustAllCerts();
                doc = Jsoup.connect(String.valueOf(url)).get();
                Utils.safeHTMLFile(doc);

                LOGGER.info("getting an html page");

            } catch (IOException e) {
                LOGGER.error(e);
            }

        String content = doc.body().select("div").text();

        Map<String, Long> wordMap = Arrays.stream(content
                .toUpperCase()
                .split("[.,;:\"!?()\\[\\]\\n\\r\\t\\s/«»\u200C]"))
                .collect(Collectors
                        .groupingBy(Function
                                .identity(), Collectors.counting()));
        wordMap.remove("");

        try {
            mapToJson = new ObjectMapper().writeValueAsString(wordMap);

            LOGGER.info("Converting map to json");
        } catch (JsonProcessingException e) {
            LOGGER.error(e);
        }

        for (Map.Entry<String, Long> item : wordMap.entrySet()) {
            System.out.printf("Key: %-30s  Value: %-30s \n",
                    item.getKey(), item.getValue());
        }
            List<Websites> listAddress = websiteService.findByAddress(url);
            if (listAddress.size() != 0
                    && listAddress.get(0).getWebsiteAddress().equals(url)) {

                Websites websites = listAddress.get(0);
                websites.setWebsiteValue(mapToJson);
                websiteService.updateWebsite(websites);
            } else {
                websiteService.addWebsite(new Websites(url, mapToJson));
            }

        } else {
            LOGGER.error("The url is unavailable or invalid");
        }
    }
}
