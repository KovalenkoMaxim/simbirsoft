package project.dao;

import project.entity.Websites;

import java.util.List;

public interface WebsiteDAO {
    Websites findById(long id);

    List<Websites> findAll();

    void addWebsite(Websites websites);

    void deleteWebsite(Websites websites);

    void updateWebsite(Websites websites);

    List<Websites> findByAddress(String websiteAddress);
}
