package project.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import project.entity.Websites;
import project.utility.HibernateSessionFactoryUtil;

import java.util.List;

public class WebsiteDAOImpl implements WebsiteDAO {

    @Override
    public Websites findById(long id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Websites websites = session.get(Websites.class, id);
        session.close();

        return websites;
    }

    @Override
    public List<Websites> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        List<Websites> websitesList = (List<Websites>) session.createQuery("From Websites ").list();
        session.close();

        return websitesList;
    }

    @Override
    public void addWebsite(Websites websites) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(websites);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteWebsite(Websites websites) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(websites);
        transaction.commit();
        session.close();
    }

    @Override
    public void updateWebsite(Websites websites) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(websites);
        transaction.commit();
        session.close();
    }

    @Override
    public List<Websites> findByAddress(String websiteAddress) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from Websites WHERE websiteAddress = :websiteAddress");
        query.setParameter("websiteAddress", websiteAddress);

        return query.list();
    }
}
