package project.service;

import project.dao.WebsiteDAO;
import project.dao.WebsiteDAOImpl;
import project.entity.Websites;

import java.util.List;

public class WebsiteServiceImpl implements WebsiteService {
    private final WebsiteDAO websiteDAO = new WebsiteDAOImpl();

    @Override
    public Websites findWebsite(long id) {

        return websiteDAO.findById(id);
    }

    @Override
    public List<Websites> findAllWebsites() {
        return websiteDAO.findAll();
    }

    @Override
    public void addWebsite(Websites websites) {
        websiteDAO.addWebsite(websites);
    }

    @Override
    public void deleteWebsite(Websites websites) {
        websiteDAO.deleteWebsite(websites);
    }

    @Override
    public void updateWebsite(Websites websites) {
        websiteDAO.updateWebsite(websites);
    }

    @Override
    public List<Websites> findByAddress(String websiteAddress) {
        return websiteDAO.findByAddress(websiteAddress);
    }
}
