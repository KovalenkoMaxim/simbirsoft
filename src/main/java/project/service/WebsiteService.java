package project.service;

import project.entity.Websites;

import java.util.List;

public interface WebsiteService {
    Websites findWebsite(long id);

    List<Websites> findAllWebsites();

    void addWebsite(Websites websites);

    void deleteWebsite(Websites websites);

    void updateWebsite(Websites websites);

    List<Websites> findByAddress(String websiteAddress);
}
