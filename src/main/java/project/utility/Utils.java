package project.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class Utils {
    private static final Logger LOGGER = LogManager.getLogger();

    //Setting the trust of all certificates to be able to download the page over https protocols
    public static void setTrustAllCerts() {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(
                    (urlHostName, session) -> true);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    //Saving an html page specifying the disk and folder from the settings file
    public static void safeHTMLFile(Document content) {
        try (InputStream input = Utils.class.getClassLoader().getResourceAsStream("cfg.properties")) {
            Properties prop = new Properties();
            prop.load(input);

            if (!new File(prop.getProperty("pathFolder")).exists()) {
                new File(prop.getProperty("pathFolder")).mkdir();
            }

            File path = new File(prop.getProperty("pathFolder")
                    + "/" + content.title().replaceAll("[\\\\/:*?\"<>|]", "") + ".html");

            try (PrintWriter pw = new PrintWriter(path)) {
                pw.println(new File(String.valueOf(content)));
                LOGGER.info("Successful saving");
            } catch (FileNotFoundException e) {
                LOGGER.error(e);
            }
        } catch (IOException e) {
            LOGGER.error(e);
        }
    }

    public static boolean urlValidation(String url) {
        setTrustAllCerts();
        HttpURLConnection connection = null;

        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestProperty("Accept-Encoding", "");
            connection.setRequestMethod("HEAD");

            return connection.getResponseCode() == HttpURLConnection.HTTP_OK;

        } catch (Exception e) {
            LOGGER.error(e);

            return false;

        } finally {
            if (connection != null) {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
