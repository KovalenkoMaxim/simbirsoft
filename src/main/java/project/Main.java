package project;

import project.parsing.ParsingHTML;
import project.parsing.ParsingHTMLImpl;

public class Main {
    public static void main(String[] args) {
        ParsingHTML parsingHTML = new ParsingHTMLImpl();
        parsingHTML.splitAndWordCount(args[0]);
    }
}
